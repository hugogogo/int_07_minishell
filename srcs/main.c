/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperrey <lperrey@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/04 05:59:26 by lperrey           #+#    #+#             */
/*   Updated: 2021/12/20 22:26:14 by lperrey          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int		g_switch_heredoc_sigint;

int	main(int argc, char *argv[])
{
	t_all	c;

	(void)argc;
	if (!init(&c, argv))
		exit_free(&c, EXIT_FAILURE);
	if (c.script_fd)
		shell_script(&c, c.script_fd);
	else if (!isatty(STDIN_FILENO))
		shell_script(&c, STDIN_FILENO);
	else
		shell_loop(&c);
	return (0);
}
