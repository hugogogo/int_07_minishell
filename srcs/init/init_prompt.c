/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_prompt.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperrey <lperrey@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/08 09:22:12 by lperrey           #+#    #+#             */
/*   Updated: 2021/12/20 22:25:46 by lperrey          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static char	*init_prompt_base(void);

int	init_prompt(t_all *c, int script_fd)
{
	if (!script_fd && isatty(STDIN_FILENO))
	{
		c->prompt_base = init_prompt_base();
		if (!c->prompt_base)
			return (0);
		c->prompt = update_prompt(c->prompt_base);
		if (!c->prompt)
			return (0);
	}
	return (1);
}

static char	*init_prompt_base(void)
{
	char	*prompt_base;
	char	*tmp;

	tmp = getenv("USER");
	if (!tmp)
		tmp = getenv("LOGNAME");
	if (!tmp)
		tmp = U_DEFAULT_USER;
	prompt_base = ft_strjoin(TERM_LIGHT_GREEN, tmp);
	if (!prompt_base)
		return (NULL);
	prompt_base = ft_strjoinfree_s1(prompt_base, "@");
	if (!prompt_base)
		return (NULL);
	tmp = getenv("NAME");
	if (!tmp)
		tmp = U_DEFAULT_NAME;
	prompt_base = ft_strjoinfree_s1(prompt_base, tmp);
	if (!prompt_base)
		return (NULL);
	prompt_base = ft_strjoinfree_s1(prompt_base, TERM_RESET":"TERM_LIGHT_BLUE);
	if (!prompt_base)
		return (NULL);
	return (prompt_base);
}

char	*update_prompt(char *prompt_base)
{
	char	*prompt;

	prompt = ft_strjoinfree_s2(prompt_base, getcwd(NULL, 0));
	if (!prompt)
		return (NULL);
	prompt = ft_strjoinfree_s1(prompt, TERM_RESET U_PROMPT_END);
	if (!prompt)
		return (NULL);
	return (prompt);
}
