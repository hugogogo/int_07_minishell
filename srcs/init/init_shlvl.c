/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_shlvl.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperrey <lperrey@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/08 09:22:12 by lperrey           #+#    #+#             */
/*   Updated: 2021/12/18 04:35:33 by lperrey          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int	init_shlvl(void)
{
	char	*tmp;
	int		ret;

	tmp = getenv("SHLVL");
	if (tmp && ft_isdigit_str(tmp))
	{
		tmp = ft_itoa(ft_atoi(tmp) + 1);
		if (!tmp)
			return (0);
		tmp = ft_strjoinfree_s2("SHLVL=", tmp);
		if (!tmp)
			return (0);
		ret = export_var(tmp);
		free(tmp);
	}
	else
		ret = export_var("SHLVL=1");
	if (ret == -1)
		return (0);
	return (1);
}
