/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   open_script_file.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperrey <lperrey@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/20 22:22:29 by lperrey           #+#    #+#             */
/*   Updated: 2021/12/20 22:27:14 by lperrey          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	open_script_file(t_all *c, char *argv[])
{
	if (argv[1])
	{
		c->script_fd = open(argv[1], O_RDONLY);
		if (c->script_fd == -1)
		{
			shell_perror(argv[1], ": ", "", 0);
			exit_free(c, EXIT_CMD_NOT_FOUND);
		}
	}
}
