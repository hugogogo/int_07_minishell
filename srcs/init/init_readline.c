/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_readline.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperrey <lperrey@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/08 09:22:12 by lperrey           #+#    #+#             */
/*   Updated: 2021/12/18 04:31:59 by lperrey          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static int	init_readline_hook(void);

void	init_readline(void)
{
	rl_outstream = stderr;
	rl_startup_hook = init_readline_hook;
	readline(NULL);
	rl_startup_hook = NULL;
}

/*
** set rl_startup_hook with this, for init COLUMNS and LINES variables
** and prevent leak/double_free with **environ
*/
static int	init_readline_hook(void)
{
	rl_done = 1;
	return (0);
}
