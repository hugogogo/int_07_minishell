/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperrey <lperrey@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/08 09:22:12 by lperrey           #+#    #+#             */
/*   Updated: 2021/12/22 19:24:04 by lperrey          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	init_readline(void);
int		init_shlvl(void);
void	open_script_file(t_all *c, char *argv[]);
int		init_prompt(t_all *c, int script_fd);

int	init(t_all *c, char *argv[])
{
	ft_bzero(c, sizeof (*c));
	set_signals_behaviour();
	init_readline();
	environ = ft_dup_2d_arr(environ, (t_dup_f)ft_strdup);
	if (!environ)
		return (ft_reti_perror(0, "ft_dup_2d_arr(environ)"));
	retrieve_path(&c->path);
	if (!init_shlvl())
		return (ft_reti_perror(0, "init_shlvl()"));
	open_script_file(c, argv);
	if (!init_prompt(c, c->script_fd))
		return (ft_reti_perror(0, "init_prompt()"));
	return (1);
}
