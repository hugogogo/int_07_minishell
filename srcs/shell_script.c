/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   shell_script.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperrey <lperrey@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/26 23:47:44 by lperrey           #+#    #+#             */
/*   Updated: 2021/12/22 14:01:52 by lperrey          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static char	*read_input_script(t_all *c, int script_fd);
static void	exit_signal(t_all *c);

void	shell_script(t_all *c, int script_fd)
{
	char	*line_input;

	line_input = NULL;
	while (1)
	{
		free(line_input);
		line_input = read_input_script(c, script_fd);
		if (!line_input)
			break ;
		if (line_input && *line_input)
		{
			c->token_list = lexing(line_input);
			ft_free_null(&line_input);
			if (!c->token_list)
				break ;
			c->pipeline = parsing(c->token_list, script_fd);
			ft_lstclear((t_list **)&c->token_list, free);
			if (!c->pipeline)
				break ;
			exec_cmd_line(c);
			if (get_last_exit_status() > EXIT_SIGNAL)
				exit_signal(c);
		}
	}
	exit_free(c, get_last_exit_status());
}

static char	*read_input_script(t_all *c, int script_fd)
{
	char				*line_input;
	struct sigaction	signal_behaviour;
	int					ret;

	ft_bzero(&signal_behaviour, sizeof signal_behaviour);
	signal_behaviour.sa_handler = SIG_IGN;
	sigaction(SIGINT, &signal_behaviour, NULL);
	line_input = NULL;
	ret = gnl(script_fd, &line_input, 0);
	if (ret == -1)
	{
		free(line_input);
		return (NULL);
	}
	if (!line_input[0] && ret == 0)
	{
		free(line_input);
		exit_free(c, get_last_exit_status());
	}
	return (line_input);
}

static void	exit_signal(t_all *c)
{
	write(STDOUT_FILENO, "\n", 1);
	exit_free(c, get_last_exit_status());
}
