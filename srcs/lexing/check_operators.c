/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_operators.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperrey <lperrey@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/19 08:38:55 by lperrey           #+#    #+#             */
/*   Updated: 2021/10/30 22:37:08 by lperrey          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static int		check_redirection(t_token *t, char *input, int *i, int *t_i);
static int		check_pipe(t_token *t, char *input, int *i, int *t_i);

int	check_operators(t_token *t, char *input, int *i, int *t_i)
{
	if (*t_i != 0 && (input[*i] == '|' || input[*i] == '<' || input[*i] == '>'))
		return (DELIMITE_TOKEN);
	else if (check_pipe(t, input, i, t_i))
		return (DELIMITE_TOKEN);
	else if (check_redirection(t, input, i, t_i))
		return (DELIMITE_TOKEN);
	return (CONTINUE_TOKEN);
}

static int	check_pipe(t_token *t, char *input, int *i, int *t_i)
{
	if (input[*i] == '|')
	{
		t->content[(*t_i)++] = input[(*i)++];
		t->id = T_PIPE;
		return (1);
	}
	return (0);
}

static int	check_redirection(t_token *t, char *input, int *i, int *t_i)
{
	if (input[*i] == '<')
	{
		t->content[(*t_i)++] = input[(*i)++];
		t->id = T_LESS;
		if (input[*i] == '<')
		{
			t->content[(*t_i)++] = input[(*i)++];
			t->id = T_DLESS;
		}
		return (1);
	}
	else if (input[*i] == '>')
	{
		t->content[(*t_i)++] = input[(*i)++];
		t->id = T_GREAT;
		if (input[*i] == '>')
		{
			t->content[(*t_i)++] = input[(*i)++];
			t->id = T_DGREAT;
		}
		return (1);
	}
	return (0);
}
