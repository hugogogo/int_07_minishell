/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperrey <lperrey@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/10 05:01:26 by lperrey           #+#    #+#             */
/*   Updated: 2021/12/01 17:19:48 by lperrey          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int	builtin_env(int argc, char *argv[], t_all *c)
{
	(void)argc;
	(void)argv;
	(void)c;
	ft_putendl_arr_fd(environ, 1);
	return (0);
}
