/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exit.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperrey <lperrey@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/10 05:01:22 by lperrey           #+#    #+#             */
/*   Updated: 2021/11/29 12:43:47 by lperrey          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int	builtin_exit(int argc, char *argv[], t_all *c)
{
	unsigned char	status;
	int				i;

	status = get_last_exit_status();
	if (argc > 2)
		return (ft_reti_print(1, "exit: too many arguments\n", 2));
	if (argc == 2)
	{
		i = 0;
		while (argv[1][i])
		{
			if ((argv[1][0] == '-' || argv[1][0] == '+') && argv[1][1] != '\0')
				i++;
			while (ft_isdigit(argv[1][i]))
				i++;
			if (argv[1][i] != '\0')
			{
				ft_putstr_fd("exit: ", 2);
				ft_putstr_fd(argv[1], 2);
				return (ft_reti_print(2, " numeric argument required\n", 2));
			}
		}
		status = ft_atoi(argv[1]);
	}
	return (exit_free(c, status));
}
