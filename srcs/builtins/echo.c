/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   echo.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperrey <lperrey@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/21 16:37:44 by lperrey           #+#    #+#             */
/*   Updated: 2021/10/21 20:22:22 by lperrey          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

enum	e_echo_options
{
	BIT_N = 01,
};

int	builtin_echo(int argc, char *argv[], t_all *c)
{
	int	i;
	int	options;

	(void)argc;
	(void)c;
	options = 0;
	i = 1;
	while (argv[i] && argv[i][0] == '-')
	{
		if (ft_isinset_str(&argv[i][1], "n"))
			options = options | BIT_N;
		else
			break ;
		i++;
	}
	while (argv[i])
	{
		ft_putstr_fd(argv[i], 1);
		i++;
		if (argv[i])
			write(1, " ", 1);
	}
	if ((options & BIT_N) == 0)
		write(1, "\n", 1);
	return (0);
}
