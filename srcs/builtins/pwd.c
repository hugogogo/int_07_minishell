/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pwd.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperrey <lperrey@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/04 19:15:36 by lperrey           #+#    #+#             */
/*   Updated: 2021/12/04 20:22:49 by lperrey          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int	builtin_pwd(int argc, char *argv[], t_all *c)
{
	char	*working_dir;

	(void)argc;
	(void)argv;
	(void)c;
	working_dir = getcwd(NULL, 0);
	if (!working_dir)
		return (ft_reti_perror(EXIT_FAILURE, "builtin_pwd, getcwd()"));
	write(1, working_dir, ft_strlen(working_dir));
	write(1, "\n", 1);
	free(working_dir);
	return (EXIT_SUCCESS);
}
