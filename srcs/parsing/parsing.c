/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parsing.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperrey <lperrey@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/24 10:52:40 by lperrey           #+#    #+#             */
/*   Updated: 2021/12/22 13:53:19 by lperrey          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	save_redirections_words(t_token *t);

t_cmd	**parsing(t_token *token_list, int script_fd)
{
	t_cmd	**pipeline;

	if (!valid_syntax(token_list))
		return (NULL);
	save_redirections_words(token_list);
	pipeline = pipeline_alloc(1 + count_pipes(token_list));
	if (!pipeline)
		return (NULL);
	if (!expansions(token_list, pipeline))
		return (ft_retp_free(NULL, &pipeline, (t_free_f)free_pipeline));
	if (!redirections(token_list, pipeline, script_fd))
		return (ft_retp_free(NULL, &pipeline, (t_free_f)free_pipeline));
	if (!pipeline_fill_argv(token_list, pipeline))
		return (ft_retp_free(NULL, &pipeline, (t_free_f)free_pipeline));
	return (pipeline);
}

void	save_redirections_words(t_token *t)
{
	while (t)
	{
		if (t->id == '>' || t->id == T_DGREAT
			|| t->id == '<' || t->id == T_DLESS)
		{
			t = t->next;
			t->id = T_REDIRECTION_WORD;
		}
		t = t->next;
	}
}

/*
	2.9.1 Simple Commands
	2.9.1 - 1) Save Words
	2.9.1 - 2) Expansion
	2.9.1 - 3) Redirection
https://pubs.opengroup.org/onlinepubs/9699919799/utilities/V3_chap02.html
#tag_18_09_01
*/

/*
	2.10 Shell Grammar
https://pubs.opengroup.org/onlinepubs/9699919799/utilities/V3_chap02.html
#tag_18_10
*/

/* -------------------------------------------------------
   The grammar symbols
   ------------------------------------------------------- */
/*
%token  WORD
%token  LESS // '<'
%token  GREAT // '>'
%token  DLESS // '<<'
%token  DGREAT // '>>'
%token  PIPE // '|'
*/

/* -------------------------------------------------------
   The Simplified Grammar
   ------------------------------------------------------- */
/*
%start program
%%
pipeline         :                   command
                 | pipeline      '|' command
                 ;
command          : cmd_prefix cmd_name cmd_suffix
                 | cmd_prefix cmd_name
                 | cmd_name cmd_suffix
                 | cmd_name
				 | cmd_prefix
                 ;
cmd_name         : WORD                   // Apply rule 7a
                 ;
cmd_prefix       :            io_redirect
                 | cmd_prefix io_redirect
                 ;
cmd_suffix       :            io_redirect
                 | cmd_suffix io_redirect
                 |            WORD
                 | cmd_suffix WORD
                 ;
io_redirect      :           io_file
                 |           io_here
                 ;
io_file          : '<'       filename
                 | '>'       filename
                 | DGREAT    filename
                 ;
filename         : WORD                      // Apply rule 2
                 ;
io_here          : DLESS     here_end
                 ;
here_end         : WORD                      // Apply rule 3
                 ;
*/
