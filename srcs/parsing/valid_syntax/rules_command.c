/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rules_command.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperrey <lperrey@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/24 18:52:05 by lperrey           #+#    #+#             */
/*   Updated: 2021/12/22 22:11:40 by hulamy           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int	valid_io_redirect(t_token **token_list);

// cmd_prefix cmd_name cmd_suffix
int	valid_command_rule1(t_token **token_list)
{
	if (!valid_io_redirect(token_list))
		return (0);
	while (valid_io_redirect(token_list))
		continue ;
	if (!valid_token(token_list, T_WORD))
		return (0);
	if (!valid_token(token_list, T_WORD) && !valid_io_redirect(token_list))
		return (0);
	while (valid_token(token_list, T_WORD) || valid_io_redirect(token_list))
		continue ;
	if (!valid_command_separator(*token_list))
		return (0);
	return (1);
}

// cmd_prefix cmd_name
int	valid_command_rule2(t_token **token_list)
{
	if (!valid_io_redirect(token_list))
		return (0);
	while (valid_io_redirect(token_list))
		continue ;
	if (!valid_token(token_list, T_WORD))
		return (0);
	if (!valid_command_separator(*token_list))
		return (0);
	return (1);
}

// cmd_name cmd_suffix
int	valid_command_rule3(t_token **token_list)
{
	if (!valid_token(token_list, T_WORD))
		return (0);
	if (!valid_token(token_list, T_WORD) && !valid_io_redirect(token_list))
		return (0);
	while (valid_token(token_list, T_WORD) || valid_io_redirect(token_list))
		continue ;
	if (!valid_command_separator(*token_list))
		return (0);
	return (1);
}

// cmd_name
int	valid_command_rule4(t_token **token_list)
{
	if (!valid_token(token_list, T_WORD))
		return (0);
	if (!valid_command_separator(*token_list))
		return (0);
	return (1);
}

// cmd_prefix
int	valid_command_rule5(t_token **token_list)
{
	if (!valid_io_redirect(token_list))
		return (0);
	while (valid_io_redirect(token_list))
		continue ;
	if (!valid_command_separator(*token_list))
		return (0);
	return (1);
}
