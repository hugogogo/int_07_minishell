/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   valid_io_redirect.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperrey <lperrey@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/24 18:52:42 by lperrey           #+#    #+#             */
/*   Updated: 2021/10/24 19:21:35 by lperrey          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static int	valid_io_file(t_token **token_list);
static int	valid_io_here(t_token **token_list);

int	valid_io_redirect(t_token **token_list)
{
	if (valid_io_file(token_list) || valid_io_here(token_list))
		return (1);
	return (0);
}

static int	valid_io_file(t_token **token_list)
{
	if (valid_token(token_list, '<') || valid_token(token_list, '>')
		|| valid_token(token_list, T_DGREAT))
	{
		if (valid_token(token_list, T_WORD))
			return (1);
	}
	return (0);
}

static int	valid_io_here(t_token **token_list)
{
	if (valid_token(token_list, T_DLESS))
	{
		if (valid_token(token_list, T_WORD))
			return (1);
	}
	return (0);
}
