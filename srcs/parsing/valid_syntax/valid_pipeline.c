/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   valid_pipeline.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperrey <lperrey@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/24 18:51:35 by lperrey           #+#    #+#             */
/*   Updated: 2021/10/24 19:35:16 by lperrey          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int	valid_command(t_token **token_list);

int	valid_pipeline(t_token **token_list)
{
	while (valid_command(token_list))
	{
		if (*token_list == NULL)
			return (1);
		else if ((*token_list)->id != '|' || (*token_list)->next == NULL)
			return (0);
		valid_token(token_list, '|');
	}
	return (0);
}
