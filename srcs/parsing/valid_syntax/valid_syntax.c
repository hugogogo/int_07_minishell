/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   valid_syntax.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperrey <lperrey@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/24 13:01:10 by lperrey           #+#    #+#             */
/*   Updated: 2021/10/24 19:40:23 by lperrey          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int	valid_pipeline(t_token **token_list);

int	valid_syntax(t_token *token_list)
{
	if (valid_pipeline(&token_list))
		return (1);
	else
	{
		ft_putstr_fd("minishell: syntax error near unexpected token \'", 2);
		ft_putstr_fd(token_list->content, 2);
		ft_putstr_fd("\'\n", 2);
	}
	return (0);
}

int	valid_token(t_token **token_list, enum e_token_id token_id)
{
	if (*token_list != NULL && (*token_list)->id == token_id)
	{
		*token_list = (*token_list)->next;
		return (1);
	}
	return (0);
}

int	valid_command_separator(const t_token *token_list)
{
	if (token_list == NULL || token_list->id == '|')
		return (1);
	return (0);
}
