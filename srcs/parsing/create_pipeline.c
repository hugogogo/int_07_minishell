/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   create_pipeline.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperrey <lperrey@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/02 22:46:23 by lperrey           #+#    #+#             */
/*   Updated: 2021/11/29 12:29:27 by lperrey          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static size_t	cmd_words_count(t_token *t);

size_t	count_pipes(t_token *t)
{
	size_t	count;

	count = 0;
	while (t)
	{
		if (t->id == T_PIPE)
			count++;
		t = t->next;
	}
	return (count);
}

t_cmd	**pipeline_alloc(size_t cmd_nbr)
{
	t_cmd	**pipeline;
	size_t	i;

	pipeline = ft_calloc(cmd_nbr + 1, sizeof (void *));
	if (!pipeline)
		return (ft_retp_perror(NULL, "pipeline_alloc()"));
	i = 0;
	while (i < cmd_nbr)
	{
		pipeline[i] = ft_calloc(1, sizeof (*pipeline[i]));
		if (!pipeline[i])
		{
			ft_free_2d_arr(pipeline);
			return (ft_retp_perror(NULL, "pipeline_alloc()"));
		}
		pipeline[i]->fd_in = STDIN_FILENO;
		pipeline[i]->fd_out = STDOUT_FILENO;
		i++;
	}
	return (pipeline);
}

int	pipeline_fill_argv(t_token *t, t_cmd **pipeline)
{
	size_t	i;
	size_t	arg_i;

	i = 0;
	while (pipeline[i])
	{
		pipeline[i]->argv = ft_calloc(cmd_words_count(t) + 1, sizeof (char *));
		if (!pipeline[i]->argv)
			return (ft_reti_perror(0, "pipeline_fill_argv()"));
		arg_i = 0;
		while (t && t->id != '|')
		{
			if (t->id == T_WORD)
			{
				pipeline[i]->argv[arg_i++] = t->content;
				t->content = NULL;
			}
			t = t->next;
		}
		if (t && t->id == '|')
			t = t->next;
		i++;
	}
	return (1);
}

static size_t	cmd_words_count(t_token *t)
{
	size_t	count;

	count = 0;
	while (t && t->id != '|')
	{
		if (t->id == T_WORD)
			count++;
		t = t->next;
	}
	return (count);
}
