/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rejoin_after_expand.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperrey <lperrey@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/07 02:01:33 by lperrey           #+#    #+#             */
/*   Updated: 2021/12/20 14:55:03 by hulamy           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

char	*rejoin_after_expand(t_list *expand_lst)
{
	t_list	*head;
	char	*result;

	head = expand_lst;
	result = NULL;
	while (expand_lst)
	{
		result = ft_strjoinfree_s1(result, expand_lst->content);
		if (!result)
		{
			perror("rejoin_after_expand() error");
			return (ft_lstclear(&head, free));
		}
		expand_lst = expand_lst->next;
	}
	ft_lstclear(&head, free);
	return (result);
}
