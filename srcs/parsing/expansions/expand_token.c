/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   expand_token.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperrey <lperrey@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/07 02:01:33 by lperrey           #+#    #+#             */
/*   Updated: 2021/12/21 01:12:42 by lperrey          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

t_list	*content_copy(char *content, int *i, int *quotes_state);
t_list	*content_expand(char *content, int *i);

t_list	*expand_token(char *content)
{
	int		quotes_state;
	int		i;
	t_list	head;
	t_list	*expand;

	head.next = NULL;
	expand = &head;
	quotes_state = 0;
	i = 0;
	while (content[i])
	{
		if (content[i] == '$')
			expand->next = content_expand(content, &i);
		else
			expand->next = content_copy(content, &i, &quotes_state);
		expand = expand->next;
		if (!expand)
		{
			perror("expand_token() error");
			return (ft_lstclear(&head.next, free));
		}
	}
	return (head.next);
}
