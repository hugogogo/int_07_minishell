/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   content_copy.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperrey <lperrey@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/07 02:01:33 by lperrey           #+#    #+#             */
/*   Updated: 2021/12/21 01:04:58 by lperrey          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static void	quotes_handling(char c, int *quotes_state);

t_list	*content_copy(char *content, int *i, int *quotes_state)
{
	int		i_exp;
	t_list	*expand;

	expand = ft_lstnew_generic(sizeof(t_list), ft_strlen(&content[*i]) + 1);
	if (!expand)
		return (NULL);
	i_exp = 0;
	while (content[*i])
	{
		if (content[*i] == '$' && *quotes_state != IN_QUOTES)
			break ;
		quotes_handling(content[*i], quotes_state);
		((char *)expand->content)[i_exp++] = content[(*i)++];
	}
	return (expand);
}

static void	quotes_handling(char c, int *quotes_state)
{
	if (c == '\'' && *quotes_state != IN_DQUOTES)
	{
		if (*quotes_state == IN_QUOTES)
			*quotes_state = 0;
		else
			*quotes_state = IN_QUOTES;
	}
	else if (c == '\"' && *quotes_state != IN_QUOTES)
	{
		if (*quotes_state == IN_DQUOTES)
			*quotes_state = 0;
		else
			*quotes_state = IN_DQUOTES;
	}
}
