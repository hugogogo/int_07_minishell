/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   here_doc.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperrey <lperrey@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/11 18:46:43 by lperrey           #+#    #+#             */
/*   Updated: 2021/12/22 17:04:47 by lperrey          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static int	here_doc_write(char *delimiter, int doc_fd, int script_fd);
static int	here_doc_write_interactive(char *delimiter, int doc_fd);
static int	here_doc_write_script(char *delimiter, int doc_fd, int script_fd);
static int	event_hook_empty(void);

#define TMP_HERE_DOC "/tmp/minishell_here_doc"
#define WARNING_EOF "warning: here-document delimited by end-of-file (wanted `"

int	here_doc(char *delimiter, int script_fd)
{
	int	here_doc;
	int	ret;

	delimiter = ft_strdup_quotes(delimiter);
	if (!delimiter)
		return (ft_reti_perror(-1, "ft_strdup_quotes()"));
	here_doc = open(TMP_HERE_DOC, O_WRONLY | O_CREAT | O_TRUNC, S_IRWXU);
	if (here_doc == -1)
		return (ft_reti_perror_io(-1, "open() ", TMP_HERE_DOC));
	ret = here_doc_write(delimiter, here_doc, script_fd);
	free(delimiter);
	if (close(here_doc) == -1)
		shell_perror(TMP_HERE_DOC, ": ", "", 0);
	if (ret != 0)
	{
		if (unlink(TMP_HERE_DOC) == -1)
			return (ft_reti_perror_io(ret, "unlink() ", TMP_HERE_DOC));
		return (ret);
	}
	here_doc = open(TMP_HERE_DOC, O_RDONLY);
	if (here_doc == -1)
		shell_perror(TMP_HERE_DOC, ": ", "", 0);
	if (unlink(TMP_HERE_DOC) == -1)
		return (ft_reti_perror_io(-1, "unlink() ", TMP_HERE_DOC));
	return (here_doc);
}

static int	here_doc_write(char *delimiter, int doc_fd, int script_fd)
{
	int					ret;
	struct sigaction	signal_action;

	if (script_fd || !isatty(STDIN_FILENO))
	{
		ret = here_doc_write_script(delimiter, doc_fd, script_fd);
	}
	else
	{
		ft_bzero(&signal_action, sizeof(signal_action));
		signal_action.sa_handler = sigint_handler_heredoc;
		sigaction(SIGINT, &signal_action, NULL);
		rl_event_hook = event_hook_empty;
		ret = here_doc_write_interactive(delimiter, doc_fd);
		rl_event_hook = NULL;
		signal_action.sa_handler = SIG_IGN;
		sigaction(SIGINT, &signal_action, NULL);
	}
	return (ret);
}

static int	here_doc_write_interactive(char *delimiter, int doc_fd)
{
	char	*line;
	size_t	line_len;

	line = NULL;
	g_switch_heredoc_sigint = 0;
	while (1)
	{
		line = readline("> ");
		if (g_switch_heredoc_sigint == 1)
			return (set_last_exit_status(EXIT_SIGNAL + SIGINT));
		if (!line)
			return (shell_error(WARNING_EOF, delimiter, "')", 0));
		line_len = ft_strlen(line);
		if (ft_strncmp(line, delimiter, line_len + 1) == 0)
			break ;
		if (write(doc_fd, line, line_len) == -1)
			return (ft_reti_perror_free(-1, line, free, "write "TMP_HERE_DOC));
		if (write(doc_fd, "\n", 1) == -1)
			return (ft_reti_perror_free(-1, line, free, "write "TMP_HERE_DOC));
		ft_free_null(&line);
	}
	free(line);
	return (0);
}

static int	here_doc_write_script(char *delimiter, int doc_fd, int script_fd)
{
	char	*line;
	int		ret;
	size_t	line_len;

	line = NULL;
	ret = 1;
	while (ret)
	{
		ret = gnl(script_fd, &line, 0);
		if (ret == -1)
			return (ft_reti_perror_free(-1, line, free, "gnl() script_fd"));
		line_len = ft_strlen(line);
		if (ft_strncmp(line, delimiter, line_len + 1) == 0)
			break ;
		if (write(doc_fd, line, line_len) == -1)
			return (ft_reti_perror_free(-1, line, free, "write "TMP_HERE_DOC));
		if ((ret || line[0]) && write(doc_fd, "\n", 1) == -1)
			return (ft_reti_perror_free(-1, line, free, "write "TMP_HERE_DOC));
		ft_free_null(&line);
	}
	if (!line && ret == 0)
		shell_error(WARNING_EOF, delimiter, "')", 0);
	free(line);
	return (0);
}

static int	event_hook_empty(void)
{
	return (0);
}

/*
https://pubs.opengroup.org/onlinepubs/9699919799/utilities/V3_chap02.html
#tag_18_07_04
https://pubs.opengroup.org/onlinepubs/9699919799/utilities/V3_chap02.html
#tag_18_02
*/
/* If any part of word is quoted,
the delimiter shall be formed by performing quote removal on word,
and the here-document lines shall not be expanded.
Otherwise, the delimiter shall be the word itself. */
// TODO Bonus: expansions in here_doc.

/*
**	rl_done
**		Setting this to a non-zero value causes Readline
**		to return the current line immediately.
**	rl_event_hook
**		If non-zero, this is the address of a function to call periodically
**		when Readline is waiting for terminal input.
**		By default, this will be called at most
**		ten times a second if there is no keyboard input.
**	
**	https://stackoverflow.com/questions/53165704/
**	readline-c-force-return-of-certain-text-in-readline
**		"rl_done is only checked in the event loop. When you give it
**		a null event hook function, it checks the rl_done and exits"
**	
**	rl_signal_event_hook
**		If non-zero, this is the address of a function to call if a read
**		system call is interrupted when Readline is reading terminal input.
*/
