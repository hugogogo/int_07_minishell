/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   redirections.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperrey <lperrey@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/11 18:46:43 by lperrey           #+#    #+#             */
/*   Updated: 2021/12/22 14:00:33 by lperrey          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int			here_doc(char *delimiter, int script_fd);

static int	redirect_cmd_input(t_token *t, t_cmd *cmd);
static int	redirect_cmd_heredoc(t_token *t, t_cmd *cmd, int script_fd);
static int	redirect_cmd_output(t_token *t, t_cmd *cmd);
static int	expand_redirection(t_token *t);

int	redirections(t_token *t, t_cmd **pipeline, int script_fd)
{
	int	i;

	i = 0;
	while (t)
	{
		if (t->id == '|')
			i++;
		if (!pipeline[i]->error)
		{
			if (t->id == '<' && !redirect_cmd_input(t, pipeline[i]))
				return (0);
			else if (t->id == T_DLESS
				&& !redirect_cmd_heredoc(t, pipeline[i], script_fd))
				return (0);
			else if (t->id == '>' || t->id == T_DGREAT)
				if (!redirect_cmd_output(t, pipeline[i]))
					return (0);
		}
		t = t->next;
	}
	return (1);
}

static int	redirect_cmd_input(t_token *t, t_cmd *cmd)
{
	if (cmd->fd_in != STDIN_FILENO)
		if (close(cmd->fd_in) == -1)
			perror("close()");
	if (!expand_redirection(t))
	{
		cmd->error = EXIT_REDIRECTION;
		return (1);
	}
	cmd->fd_in = open(t->next->content, O_RDONLY);
	if (cmd->fd_in == -1)
	{
		shell_perror(t->next->content, ": ", "", 0);
		cmd->error = EXIT_REDIRECTION;
	}
	return (1);
}

static int	redirect_cmd_heredoc(t_token *t, t_cmd *cmd, int script_fd)
{
	if (cmd->fd_in != STDIN_FILENO)
		if (close(cmd->fd_in) == -1)
			perror("close()");
	cmd->fd_in = here_doc(t->next->content, script_fd);
	if (cmd->fd_in == -1)
	{
		shell_error("heredoc error", ": ", "", 0);
		cmd->error = EXIT_REDIRECTION;
	}
	else if (cmd->fd_in > EXIT_SIGNAL)
	{
		cmd->fd_in = 0;
		return (0);
	}
	return (1);
}

static int	redirect_cmd_output(t_token *t, t_cmd *cmd)
{
	int	flags;

	if (cmd->fd_out != STDOUT_FILENO)
		if (close(cmd->fd_out) == -1)
			perror("close()");
	if (!expand_redirection(t))
	{
		cmd->error = EXIT_REDIRECTION;
		return (1);
	}
	flags = O_WRONLY | O_CREAT;
	if (t->id == '>')
		flags = flags | O_TRUNC;
	else if (t->id == T_DGREAT)
		flags = flags | O_APPEND;
	cmd->fd_out = open(t->next->content, flags, S_IRWXU);
	if (cmd->fd_out == -1)
	{
		shell_perror(t->next->content, ": ", "", EXIT_REDIRECTION);
		cmd->error = EXIT_REDIRECTION;
	}
	return (1);
}

static int	expand_redirection(t_token *t)
{
	t_token	*head;
	t_token	*next_token;
	int		ret;

	ret = 1;
	head = t;
	t = t->next;
	next_token = t->next;
	t->next = NULL;
	if (!token_expansions(t))
	{
		((t_token *)ft_lstlast((t_list *)head))->next = next_token;
		return (0);
	}
	head->next = t->next;
	free(t);
	if (head->next)
		head->next->id = T_REDIRECTION_WORD;
	if (ft_lstsize((t_list *)head->next) != 1)
	{
		ret = 0;
		shell_error("", "", "ambiguous redirect", 0);
	}
	((t_token *)ft_lstlast((t_list *)head))->next = next_token;
	return (ret);
}
