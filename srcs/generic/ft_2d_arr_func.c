/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_2d_arr_func.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperrey <lperrey@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/08 09:25:35 by lperrey           #+#    #+#             */
/*   Updated: 2021/12/16 03:43:33 by lperrey          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

// Replace ft_arrlen()
size_t	ft_2d_arrlen(void *ptr)
{
	size_t	len;
	char	**arr;

	arr = (char **)ptr;
	len = 0;
	while (arr[len] != NULL)
		len++;
	return (len);
}

void	*ft_dup_2d_arr(void	*ptr, void *(*dup_func)(void *))
{
	unsigned int	i;
	char			**arr;
	char			**new_arr;

	new_arr = ft_calloc(ft_2d_arrlen(ptr) + 1, sizeof (void *));
	if (!new_arr)
		return (NULL);
	arr = (char **)ptr;
	i = 0;
	while (arr[i])
	{
		new_arr[i] = dup_func(arr[i]);
		if (!new_arr[i])
			return (ft_retp_free(NULL, new_arr, ft_free_2d_arr));
		i++;
	}
	return (new_arr);
}

void	*ft_resize_2d_arr(void *ptr, size_t add_nbr)
{
	unsigned int	i;
	char			**arr;
	char			**new_arr;

	new_arr = ft_calloc(ft_2d_arrlen(ptr) + add_nbr + 1, sizeof (void *));
	if (!new_arr)
		return (NULL);
	arr = (char **)ptr;
	i = 0;
	while (arr[i])
	{
		new_arr[i] = arr[i];
		i++;
	}
	free(arr);
	return (new_arr);
}
