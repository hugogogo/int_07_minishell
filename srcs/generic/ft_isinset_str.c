/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isinset_str.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperrey <lperrey@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/08 09:25:35 by lperrey           #+#    #+#             */
/*   Updated: 2021/12/16 03:45:50 by lperrey          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int	ft_isinset_str(char *str, char *set)
{
	size_t	i;
	size_t	i_set;
	int		valid;

	i = 0;
	while (str[i])
	{
		valid = 0;
		i_set = 0;
		while (set[i_set] && !valid)
		{
			if (str[i] == set[i_set])
				valid = 1;
			i_set++;
		}
		if (!valid)
			return (0);
		i++;
	}
	return (i);
}
