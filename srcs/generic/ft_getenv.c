/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_getenv.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperrey <lperrey@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/08 09:25:35 by lperrey           #+#    #+#             */
/*   Updated: 2021/12/22 18:07:16 by lperrey          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

/*
** Search for an environement variable,
** and return a pointer to the first character after "env_var=" .
** Return NULL if env_var not found.
*/
char	*ft_getenv(char *env_var)
{
	int		i;
	char	*tmp;
	size_t	env_var_len;

	env_var_len = ft_strlen(env_var);
	i = 0;
	tmp = NULL;
	while (!tmp && environ[i])
	{
		if (environ[i][env_var_len] == '=')
		{
			if (ft_strncmp(environ[i], env_var, env_var_len) == 0)
				tmp = &environ[i][env_var_len + 1];
			else
				i++;
		}
		else
			i++;
	}
	return (tmp);
}

/*
** Like ft_getenv(), but return position of env_var instead of value.
** If env_var not found, return last position of **environ (== NULL)
*/
size_t	ft_getenv_position(char *env_var)
{
	int		i;
	int		found;
	size_t	env_var_len;

	env_var_len = ft_strlen(env_var);
	i = 0;
	found = 0;
	while (!found && environ[i])
	{
		if (ft_strncmp(environ[i], env_var, env_var_len) == 0)
		{
			if (environ[i][env_var_len] == '=')
				found = 1;
			else
				i++;
		}
		else
			i++;
	}
	return (i);
}
