/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lst_func.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperrey <lperrey@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/08 09:25:35 by lperrey           #+#    #+#             */
/*   Updated: 2021/12/16 03:45:39 by lperrey          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	ft_lstprint(t_list *lst, int fd)
{
	while (lst)
	{
		ft_putendl_fd(lst->content, fd);
		lst = lst->next;
	}
}

t_list	*ft_lstbeforelast(t_list *lst)
{
	if (!lst || !lst->next)
		return (NULL);
	while (lst->next->next)
		lst = lst->next;
	return (lst);
}

/* if "content_size == 0", return lst with "lst->content == NULL" */
void	*ft_lstnew_generic(size_t lst_size, size_t content_size)
{
	t_list	*elem;
	void	*content;

	if (content_size == 0)
		content = NULL;
	else
	{
		content = ft_calloc(content_size, 1);
		if (!content)
			return (NULL);
	}
	elem = ft_calloc(1, lst_size);
	if (!elem)
	{
		free(content);
		return (NULL);
	}
	elem->content = content;
	elem->next = NULL;
	return (elem);
}
