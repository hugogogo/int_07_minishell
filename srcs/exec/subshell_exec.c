/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   subshell_exec.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperrey <lperrey@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/16 01:57:38 by lperrey           #+#    #+#             */
/*   Updated: 2021/12/22 22:12:16 by hulamy           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static void	exec_builtin(t_cmd *cmd, t_all *c);

int	cmd_exec_in_subshell(t_cmd *cmd, t_all *c)
{
	struct sigaction	signal_behaviour;

	cmd->pid = fork();
	if (cmd->pid == -1)
		perror("fork()");
	if (cmd->pid == 0)
	{
		ft_bzero(&signal_behaviour, sizeof signal_behaviour);
		signal_behaviour.sa_handler = SIG_DFL;
		sigaction(SIGINT, &signal_behaviour, NULL);
		sigaction(SIGQUIT, &signal_behaviour, NULL);
		if (cmd->fd_in != STDIN_FILENO)
			if (dup2(cmd->fd_in, STDIN_FILENO) == -1)
				return (ft_reti_perror(EXIT_FAILURE, "dup2()"));
		if (cmd->fd_out != STDOUT_FILENO)
			if (dup2(cmd->fd_out, STDOUT_FILENO) == -1)
				return (ft_reti_perror(EXIT_FAILURE, "dup2()"));
		close_pipeline_fd(c->pipeline);
		if (cmd->builtin_f)
			exec_builtin(cmd, c);
		else if (execve(cmd->path, cmd->argv, environ) == -1)
			return (ft_reti_perror_io(EXIT_FAILURE, "execve() ", cmd->argv[0]));
	}
	return (EXIT_SUCCESS);
}

static void	exec_builtin(t_cmd *cmd, t_all *c)
{
	struct sigaction	signal_behaviour;
	int					ret;

	ft_bzero(&signal_behaviour, sizeof signal_behaviour);
	signal_behaviour.sa_handler = SIG_IGN;
	sigaction(SIGPIPE, &signal_behaviour, NULL);
	ret = cmd->builtin_f(ft_2d_arrlen(cmd->argv), cmd->argv, c);
	exit_free(c, ret);
}
