/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   subshell_wait.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperrey <lperrey@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/16 01:57:38 by lperrey           #+#    #+#             */
/*   Updated: 2021/12/22 15:38:01 by lperrey          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static void	handle_signal_status(int wstatus);
static int	handle_wait_error(void);

void	wait_subshell(pid_t last_cmd_pid)
{
	int	wstatus;
	int	ret;

	wstatus = 0;
	if (last_cmd_pid > 0)
	{
		if (waitpid(last_cmd_pid, &wstatus, 0) == -1)
			perror("waitpid()");
		if (WIFEXITED(wstatus))
			set_last_exit_status(WEXITSTATUS(wstatus));
		if (WIFSIGNALED(wstatus))
			handle_signal_status(wstatus);
	}
	ret = 0;
	while (ret != -1)
	{
		ret = wait(&wstatus);
		if (ret == -1)
			ret = handle_wait_error();
	}
	if (get_last_exit_status() == EXIT_SIGNAL + SIGSEGV)
		ft_putstr_fd("Segmentation fault (core dumped)\n", STDERR_FILENO);
}

static void	handle_signal_status(int wstatus)
{
	int	signum;

	signum = WTERMSIG(wstatus);
	set_last_exit_status(EXIT_SIGNAL + signum);
	if (signum == SIGINT)
		write(STDOUT_FILENO, "\n", 1);
	if (signum == SIGQUIT)
		ft_putstr_fd("Quit (core dumped)\n", STDERR_FILENO);
}

static int	handle_wait_error(void)
{
	if (errno == ECHILD)
		return (-1);
	else if (errno == EINTR)
		return (0);
	else
		perror("wait()");
	return (-1);
}
