/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pipeline.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperrey <lperrey@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/16 01:57:38 by lperrey           #+#    #+#             */
/*   Updated: 2021/12/20 18:08:16 by lperrey          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static int		open_pipes(t_cmd *pipeline[]);
static int		pipeline_find_access(t_cmd *pipeline[], char *path[]);
static pid_t	pipeline_exec(t_cmd *pipeline[], t_all *c);

int	pipeline(t_all *c)
{
	t_cmd	**pipeline;
	int		ret;

	pipeline = c->pipeline;
	if (!open_pipes(pipeline))
		return (0);
	if (!pipeline_find_access(pipeline, c->path))
		return (0);
	if (ft_2d_arrlen(pipeline) > 1)
		wait_subshell(pipeline_exec(pipeline, c));
	else
	{
		if (pipeline[0]->error)
			set_last_exit_status(pipeline[0]->error);
		else if (pipeline[0]->builtin_f)
		{
			ret = simple_command_builtin(pipeline[0], c);
			if (ret != EXIT_SUCCESS)
				set_last_exit_status(ret);
		}
		else
			wait_subshell(pipeline_exec(pipeline, c));
	}
	free_pipeline(&c->pipeline);
	return (1);
}

static int	open_pipes(t_cmd *pipeline[])
{
	int	i;
	int	pipes[2];

	i = 0;
	while (pipeline[i] && pipeline[i + 1])
	{
		if (pipe(pipes) == -1)
			return (ft_reti_perror(0, "pipe()"));
		if (pipeline[i]->fd_out == STDOUT_FILENO)
			pipeline[i]->fd_out = pipes[STDOUT_FILENO];
		else
			if (close(pipes[STDOUT_FILENO]) == -1)
				perror("close()");
		if (pipeline[i + 1]->fd_in == STDIN_FILENO)
			pipeline[i + 1]->fd_in = pipes[STDIN_FILENO];
		else
			if (close(pipes[STDIN_FILENO]) == -1)
				perror("close()");
		i++;
	}
	return (1);
}

static int	pipeline_find_access(t_cmd *pipeline[], char *path[])
{
	int	i;

	i = 0;
	while (pipeline[i])
	{
		if (!pipeline[i]->error && pipeline[i]->argv[0])
		{
			if (!cmd_find_access(pipeline[i], path))
				return (0);
		}
		i++;
	}
	return (1);
}

static pid_t	pipeline_exec(t_cmd *pipeline[], t_all *c)
{
	int	i;
	int	ret;

	i = 0;
	while (pipeline[i])
	{
		if (!pipeline[i]->error && pipeline[i]->argv[0])
		{
			ret = cmd_exec_in_subshell(pipeline[i], c);
			if (ret != EXIT_SUCCESS)
				exit_free(c, ret);
		}
		close_cmd_fd(pipeline[i]);
		i++;
	}
	i -= 1;
	if (pipeline[i]->error)
		set_last_exit_status(pipeline[i]->error);
	return (pipeline[i]->pid);
}
