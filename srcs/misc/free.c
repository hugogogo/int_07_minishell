/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperrey <lperrey@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/10 23:53:17 by lperrey           #+#    #+#             */
/*   Updated: 2021/12/21 12:14:43 by lperrey          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int	exit_free(t_all *c, int exit_status)
{
	free(c->prompt_base);
	free(c->prompt);
	ft_lstclear((t_list **)&c->token_list, free);
	ft_free_2d_arr(environ);
	ft_free_2d_arr(c->path);
	free_pipeline(&c->pipeline);
	if (c->script_fd)
	{
		if (c->script_fd > 0)
		{
			gnl(c->script_fd, NULL, 1);
			if (close(c->script_fd) == -1)
				perror("close()");
		}
	}
	else if (!isatty(STDIN_FILENO))
		gnl(STDIN_FILENO, NULL, 1);
	else
		rl_clear_history();
	close_stdio();
	exit(exit_status);
}

void	free_pipeline(t_cmd **pipeline_ptr[])
{
	int		i;
	t_cmd	**pipeline;

	pipeline = *pipeline_ptr;
	if (!pipeline)
		return ;
	close_pipeline_fd(pipeline);
	i = 0;
	while (pipeline[i])
	{
		ft_free_2d_arr(pipeline[i]->argv);
		free(pipeline[i]->path);
		i++;
	}
	ft_free_2d_arr(pipeline);
	*pipeline_ptr = NULL;
}

void	close_pipeline_fd(t_cmd *pipeline[])
{
	int		i;

	if (!pipeline)
		return ;
	i = 0;
	while (pipeline[i])
	{
		close_cmd_fd(pipeline[i]);
		i++;
	}
}

void	close_cmd_fd(t_cmd *cmd)
{
	if (cmd->fd_in != STDIN_FILENO && cmd->fd_in > 0)
	{
		if (close(cmd->fd_in) == -1)
			perror("close()");
		cmd->fd_in = 0;
	}
	if (cmd->fd_out != STDOUT_FILENO && cmd->fd_out > 0)
	{
		if (close(cmd->fd_out) == -1)
			perror("close()");
		cmd->fd_out = 0;
	}
}

void	close_stdio(void)
{
	if (close(STDIN_FILENO) == -1)
		perror("close()");
	if (close(STDOUT_FILENO) == -1)
		perror("close()");
	if (close(STDERR_FILENO) == -1)
		perror("close()");
}
