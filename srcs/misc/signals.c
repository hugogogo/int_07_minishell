/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   signals.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperrey <lperrey@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/23 18:56:53 by lperrey           #+#    #+#             */
/*   Updated: 2021/12/18 14:21:47 by lperrey          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	sigint_handler_interactive(int signum)
{
	write(1, "\n", 1);
	rl_on_new_line();
	rl_replace_line("", 1);
	rl_redisplay();
	set_last_exit_status(EXIT_SIGNAL + signum);
}

void	sigint_handler_heredoc(int signum)
{
	(void)signum;
	rl_done = 1;
	g_switch_heredoc_sigint = 1;
}

void	set_signals_behaviour(void)
{
	struct sigaction	signal_behaviour;

	ft_bzero(&signal_behaviour, sizeof signal_behaviour);
	signal_behaviour.sa_handler = SIG_IGN;
	sigaction(SIGQUIT, &signal_behaviour, NULL);
	sigaction(SIGINT, &signal_behaviour, NULL);
}
