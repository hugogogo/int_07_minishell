/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   retrieve_path.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperrey <lperrey@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/08 09:22:12 by lperrey           #+#    #+#             */
/*   Updated: 2021/12/22 19:28:49 by lperrey          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

#define WARNING_PATH "Warning, $PATH not set\n"

int	retrieve_path(char **path_stock[])
{
	char	*path;
	char	**path_split;

	path = getenv("PATH");
	if (!path)
	{
		ft_free_2d_arr(*path_stock);
		*path_stock = NULL;
		return (shell_error(WARNING_PATH, "", "", 0));
	}
	path_split = ft_split(path, ':');
	if (!path_split)
		return (ft_reti_perror(-1, "retrieve_path()"));
	ft_free_2d_arr(*path_stock);
	*path_stock = path_split;
	return (1);
}
