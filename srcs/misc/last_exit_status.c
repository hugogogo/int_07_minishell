/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   last_exit_status.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperrey <lperrey@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/26 19:02:27 by lperrey           #+#    #+#             */
/*   Updated: 2021/12/20 16:48:02 by hulamy           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int	set_last_exit_status(int new_value)
{
	static int	last_exit_status = 0;

	if (new_value >= 0)
		last_exit_status = new_value;
	return (last_exit_status);
}

int	get_last_exit_status(void)
{
	return (set_last_exit_status(-1));
}
