/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error_wrappers.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperrey <lperrey@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/01 17:16:30 by lperrey           #+#    #+#             */
/*   Updated: 2021/12/16 04:38:05 by lperrey          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int	shell_error(char *s1, char *s2, char *s3, int ret_val)
{
	char	*prefix;

	prefix = "minishell: ";
	write(STDERR_FILENO, prefix, ft_strlen(prefix));
	if (s1)
		write(STDERR_FILENO, s1, ft_strlen(s1));
	if (s2)
		write(STDERR_FILENO, s2, ft_strlen(s2));
	if (s3)
		write(STDERR_FILENO, s3, ft_strlen(s3));
	write(STDERR_FILENO, "\n", 1);
	return (ret_val);
}

int	shell_perror(char *s1, char *s2, char *s3, int ret_val)
{
	char	*prefix;

	prefix = "minishell: ";
	write(STDERR_FILENO, prefix, ft_strlen(prefix));
	if (s1)
		write(STDERR_FILENO, s1, ft_strlen(s1));
	if (s2)
		write(STDERR_FILENO, s2, ft_strlen(s2));
	if (s3)
		write(STDERR_FILENO, s3, ft_strlen(s3));
	perror(NULL);
	return (ret_val);
}

int	ft_reti_perror_io(int ret, char *err_str, char *io_file)
{
	ft_putstr_fd(err_str, STDERR_FILENO);
	perror(io_file);
	return (ret);
}
