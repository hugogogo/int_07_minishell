
# include <stdio.h>
# include <stdlib.h>

int	main(int argc, char *argv[], char *envp[])
{
	int i;

	printf("argc = %i\n", argc);
	i = 0;
	while (argv[i])
	{
		printf("%s = |%s|\n", argv[i], getenv(argv[i]));
		i++;
	}
	return (0);
}
