/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell_term_colors.h                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperrey <lperrey@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/08 02:37:57 by lperrey           #+#    #+#             */
/*   Updated: 2021/10/08 02:51:13 by lperrey          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MINISHELL_TERM_COLORS_H
# define MINISHELL_TERM_COLORS_H

// https://chrisyeh96.github.io/2020/03/28/terminal-colors.html

// Macro RL_PROMPT_START_IGNORE == '\001'
// Macro RL_PROMPT_END_IGNORE == '\002'
# define TERM_RESET "\001\e[0m\002"
# define TERM_BLACK "\001\e[0;30m\002"
# define TERM_GRAY "\001\e[1;30m\002"
# define TERM_RED "\001\e[0;31m\002"
# define TERM_LIGHT_RED "\001\e[1;31m\002"
# define TERM_GREEN "\001\e[0;32m\002"
# define TERM_LIGHT_GREEN "\001\e[1;32m\002"
# define TERM_BROWN "\001\e[0;33m\002"
# define TERM_YELLOW "\001\e[1;33m\002"
# define TERM_BLUE "\001\e[0;34m\002"
# define TERM_LIGHT_BLUE "\001\e[1;34m\002"
# define TERM_PURPLE "\001\e[0;35m\002"
# define TERM_LIGHT_PURPLE "\001\e[1;35m\002"
# define TERM_CYAN "\001\e[0;36m\002"
# define TERM_LIGHT_CYAN "\001\e[1;36m\002"
# define TERM_LIGHT_GRAY "\001\e[0;37m\002"
# define TERM_WHITE "\001\e[1;37m\002"

#endif
