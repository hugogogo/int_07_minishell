/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell_structs.h                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperrey <lperrey@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/08 02:35:52 by lperrey           #+#    #+#             */
/*   Updated: 2021/12/20 22:13:45 by lperrey          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MINISHELL_STRUCTS_H
# define MINISHELL_STRUCTS_H

struct			s_all;

typedef struct s_token
{
	char			*content;
	struct s_token	*next;
	enum e_token_id	id;
}				t_token;

typedef int		(*t_builtin_f)(int,char **,struct s_all *);

typedef struct s_cmd
{
	char		**argv;
	char		*path;
	t_builtin_f	builtin_f;
	int			fd_in;
	int			fd_out;
	pid_t		pid;
	int			error;
}				t_cmd;

typedef struct s_all
{
	t_cmd				**pipeline;
	char				**path;
	char				*prompt_base;
	char				*prompt;
	t_token				*token_list;
	int					script_fd;
}				t_all;

#endif
