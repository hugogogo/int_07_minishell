/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperrey <lperrey@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/03 19:14:46 by lperrey           #+#    #+#             */
/*   Updated: 2021/11/27 09:09:31 by lperrey          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MINISHELL_H
# define MINISHELL_H
# include "libft.h"
# include <fcntl.h>
# include <unistd.h>
# include <sys/ioctl.h>
# include <stdlib.h>
# include <string.h>
# include <errno.h>
# include <sys/wait.h>
# include <sys/stat.h>
# include <sys/types.h>
# include <sys/time.h>
# include <sys/resource.h>
# include <signal.h>
# include <dirent.h>
# include <termios.h>

// sudo apt install libncurses-dev (OR libncurses5-dev)
# include <curses.h>
# include <term.h>

// sudo apt install libreadline-dev
# include <readline/readline.h>
# include <readline/history.h>

# include <stdio.h>

# include "minishell_macro.h"
# include "minishell_structs.h"
# include "minishell_term_colors.h"
# include "minishell_user_macro.h"
# include "minishell_prototypes.h"

/*
** <stdio.h>:		printf(), perror(), readline()
** <unistd.h>:		access(), unlink()
** <signal.h>:		sigaction()
** <readline/readline.h>,
** <readline/history.h>,
**					Readline Library (-lreadline),
**					readline(), rl_clear_history(), rl_on_new_line(),
**					rl_replace_line(), rl_redisplay(), add_history()
** ------------------
** <fcntl.h>:		open()
** <unistd.h>:		read(), write(), close(), fork(), getcwd(), chdir(),
** 					stat(), lstat(), fstat(), execve(), dup(), dup2(), pipe(),
**					isatty(), ttyname(), ttyslot(), tcsetattr(), tcgetattr()
** <sys/ioctl.h>:	ioctl()
** <stdlib.h>:		malloc(), free(), exit(), getenv()
** <string.h>:		strerror(), define NULL, define size_t
** <errno.h>:		define errno
** <sys/wait.h>:	wait(), waitpid()
** <sys/resource.h>,
** <sys/time.h>:	wait3(), wait4()
** <signal.h>:		signal(), kill()
** <dirent.h>:		opendir(), readdir(), closedir()
** <termios.h>:		tcsetattr(), tcgetattr()
** <curses.h>,
** <term.h>:		Termcap Library (-ltermcap)
**					tgetent(), tgetflag(), tgetnum(), tgetstr(),
**					tgoto(), tputs()
** ------------------
** <sys/stat.h>:	open(), stat(), lstat(), fstat()
** <sys/types.h>:	open(), wait(), waitpid(), wait3(), wait4(), kill()
** 					stat(), lstat(), fstat(), opendir(), closedir()
*/

#endif
