/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell_user_macro.h                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperrey <lperrey@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/10 03:36:37 by lperrey           #+#    #+#             */
/*   Updated: 2021/10/19 20:30:34 by lperrey          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MINISHELL_USER_MACRO_H
# define MINISHELL_USER_MACRO_H

# include "minishell_macro.h"

# define U_PROMPT_END	PROMPT_CHEVRON
# define U_DEFAULT_USER	"NoUser"
# define U_DEFAULT_NAME	"NoName"

#endif
