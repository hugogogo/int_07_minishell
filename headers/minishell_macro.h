/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell_macro.h                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperrey <lperrey@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/08 02:35:55 by lperrey           #+#    #+#             */
/*   Updated: 2021/11/27 10:43:43 by lperrey          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MINISHELL_MACRO_H
# define MINISHELL_MACRO_H

// Macro RL_PROMPT_START_IGNORE == '\001'
// Macro RL_PROMPT_END_IGNORE == '\002'
# define PROMPT_CHEVRON "> "
# define PROMPT_EURO "\001€\002  \001\b\002"

# define EXIT_CMD_NOT_FOUND 127
# define EXIT_CMD_NOT_EXE 126
# define EXIT_SIGNAL 128

// 1 and 125 inclusive
# define EXIT_REDIRECTION 22
# define EXIT_EXPANSION 33

enum	e_lexer_return
{
	CONTINUE_TOKEN = 1,
	DELIMITE_TOKEN
};

enum	e_token_id
{
	T_LESS = '<',
	T_GREAT = '>',
	T_PIPE = '|',
	T_DLESS,
	T_DGREAT,
	T_WORD,
	T_REDIRECTION_WORD
};
// T_DLESS == '<<'
// T_DGREAT == '>>'

enum	e_quotes_state
{
	IN_QUOTES = '\'',
	IN_DQUOTES = '\"'
};

#endif
