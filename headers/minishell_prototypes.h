/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell_prototypes.h                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperrey <lperrey@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/08 02:59:58 by lperrey           #+#    #+#             */
/*   Updated: 2021/12/22 19:28:07 by lperrey          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MINISHELL_PROTOTYPES_H
# define MINISHELL_PROTOTYPES_H

extern int		g_switch_heredoc_sigint;
extern char		**environ;

// Init
int		init(t_all *c, char *argv[]);
char	*update_prompt(char *prompt_base);
int		retrieve_path(char **path_stock[]);
void	set_signals_behaviour(void);

// Shell modes
void	shell_loop(t_all *c);
void	shell_script(t_all *c, int script_fd);

// Lexer
t_token	*lexing(char *input);

// Parser
t_cmd	**parsing(t_token *token_list, int script_fd);
int		valid_syntax(t_token *token_list);
int		valid_token(t_token **token_list, enum e_token_id token_id);
int		valid_command_separator(const t_token *token_list);
size_t	count_pipes(t_token *token_list);
t_cmd	**pipeline_alloc(size_t cmd_nbr);
int		pipeline_fill_argv(t_token *token_list, t_cmd **pipeline);
int		expansions(t_token *token_list, t_cmd **pipeline);
int		token_expansions(t_token *t);
int		redirections(t_token *token_list, t_cmd **pipeline, int script_fd);

// Exec
int		exec_cmd_line(t_all *c);
int		pipeline(t_all *c);
int		cmd_find_access(t_cmd *cmd, char *path[]);
int		cmd_exec_in_subshell(t_cmd *cmd, t_all *c);
void	wait_subshell(pid_t last_cmd_pid);
int		simple_command_builtin(t_cmd *cmd, t_all *c);

// Builtins
int		builtin_cd(int argc, char *argv[], t_all *c);
int		builtin_pwd(int argc, char *argv[], t_all *c);
int		builtin_export(int argc, char *argv[], t_all *c);
int		export_var(char *arg);
int		builtin_unset(int argc, char *argv[], t_all *c);
int		builtin_exit(int argc, char *argv[], t_all *c);
int		builtin_env(int argc, char *argv[], t_all *c);
int		builtin_echo(int argc, char *argv[], t_all *c);

// Free
int		exit_free(t_all *c, int exit_status);
void	free_pipeline(t_cmd **pipeline_ptr[]);
void	close_pipeline_fd(t_cmd *pipeline[]);
void	close_cmd_fd(t_cmd *cmd);
void	close_stdio(void);
typedef void	(*t_free_f)(void *); // generic

// Error wrappers
int		shell_error(char *s1, char *s2, char *s3, int ret_val);
int		shell_perror(char *s1, char *s2, char *s3, int ret_val);
int		ft_reti_perror_io(int ret, char *err_str, char *io_file);

// Generic
char	*ft_strjoinfree(char *s1, char *s2);
char	*ft_strjoinfree_s1(char *s1, const char *s2);
char	*ft_strjoinfree_s2(const char *s1, char *s2);
void	ft_lstprint(t_list *lst, int fd);
int		ft_isinset_str(char *str, char *set);
size_t	ft_2d_arrlen(void *ptr); // Replace ft_arrlen()
char	**ft_dup_2d_char_arr(char **ptr);
void	*ft_resize_2d_arr(void *ptr, size_t add_nbr);
t_list	*ft_lstbeforelast(t_list *lst);
void	*ft_lstnew_generic(size_t lst_size, size_t content_size);
typedef void	*(*t_dup_f)(void *);
void	*ft_dup_2d_arr(void	*ptr, void *(*dup_func)(void *));
void	ft_free_null(void *ptr);
char	*ft_getenv(char *env_var);
size_t	ft_getenv_position(char *env_var);
int		ft_is_posix_name(char *str);

char	**ft_split_quotes(char const *s, char c);
char	*ft_strdup_quotes(const char *s);

// Signals handler
void	sigint_handler_interactive(int signum);
void	sigint_handler_heredoc(int signum);

// last_exit_status
int		set_last_exit_status(int new_value);
int		get_last_exit_status(void);

#endif
