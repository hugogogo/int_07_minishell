NAME = minishell

CC = clang

CFLAGS = -Wall -Wextra -Werror $(INCLUDES) # add -g

VPATH  = $(DIR_SRCS)
DIR_SRCS =	srcs \
			srcs/init \
			srcs/builtins \
			srcs/lexing \
			srcs/parsing srcs/parsing/valid_syntax \
			srcs/parsing/expansions srcs/parsing/redirections \
			srcs/exec \
			srcs/generic \
			srcs/misc \

INCLUDES = -I$(HEADERS_D) -I$(LIBFT_D)

HEADERS_D = ./headers
HEADERS =	minishell.h \
			minishell_structs.h minishell_prototypes.h \
			minishell_macro.h minishell_term_colors.h \
			minishell_user_macro.h

LIBS =	-L $(LIBFT_D) -lft \
		-lreadline -ltermcap

LIBFT_D = ./libft
LIBFT = $(LIBFT_D)/libft.a

SRCS =	main.c \
		shell_loop.c shell_script.c \
		init.c init_prompt.c init_readline.c init_shlvl.c open_script_file.c \
		retrieve_path.c \
		free.c \
		signals.c error_wrappers.c last_exit_status.c \
		lexing.c fill_token.c check_operators.c \
		parsing.c create_pipeline.c \
		valid_syntax.c valid_pipeline.c valid_command.c rules_command.c valid_io_redirect.c \
		expansions.c \
		expand_token.c content_expand.c content_copy.c \
		rejoin_after_expand.c new_token_for_each_field.c \
		redirections.c here_doc.c \
		exec_cmd_line.c pipeline.c \
		find_access.c subshell_exec.c subshell_wait.c simple_cmd_builtin.c \
		cd.c pwd.c export.c unset.c exit.c env.c echo.c \
		ft_split_quotes.c ft_strdup_quotes.c \
		ft_strjoinfree.c ft_lst_func.c ft_isinset_str.c ft_is_posix_name.c \
		ft_getenv.c ft_free_null.c ft_2d_arr_func.c \

DIR_OBJS = builds
OBJS = $(SRCS:%.c=$(DIR_OBJS)/%.o)

# --------------------
# ------ RULES -------
# --------------------

all: subsystem $(NAME)

subsystem:
	@cd $(LIBFT_D) && $(MAKE)

$(LIBFT): # dispensable. utile seulement pour un appel direct à $(NAME), si $(LIBFT) n'existe pas
	cd $(LIBFT_D) && $(MAKE)

$(DIR_OBJS)/%.o: %.c | $(DIR_OBJS)
	$(CC) $(CFLAGS) -c $< -o $@

$(DIR_OBJS):
	mkdir $@

$(OBJS): $(HEADERS:%=$(HEADERS_D)/%)

$(NAME): $(OBJS) $(LIBFT)
	$(CC) $(OBJS) -o $(NAME) $(LIBS)

clean:
	rm -f $(OBJS)

fclean: clean
	rm -f $(NAME)

re: fclean all

valgrind: $(NAME)
	valgrind --leak-check=full --leak-resolution=low --show-reachable=yes ./$(NAME)

run: $(NAME)
	./$(NAME)

.PHONY : all clean fclean re bonus subsystem run valgrind
